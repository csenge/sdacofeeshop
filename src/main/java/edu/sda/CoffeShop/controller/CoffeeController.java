package edu.sda.CoffeShop.controller;

import edu.sda.CoffeShop.service.CoffeeService;
import edu.sda.CoffeShop.service.LatteService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

@Controller
public class CoffeeController {

    @Autowired
    CoffeeService coffeeService;

    @Autowired
    LatteService latteService;

    public void makeCoffee(String coffeeType) {
        if (coffeeType.isEmpty()) {
            coffeeService.makeCoffee();
        } else {
            latteService.makeLatteMachiato();
        }
    }
}
