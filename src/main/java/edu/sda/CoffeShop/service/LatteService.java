package edu.sda.CoffeShop.service;

import org.springframework.stereotype.Service;

@Service
public class LatteService {

    public void makeLatteMachiato() {
        System.out.println("Making a latte");
    }
}
