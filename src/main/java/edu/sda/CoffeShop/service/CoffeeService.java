package edu.sda.CoffeShop.service;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

@Service
public class CoffeeService {

    @Value("${defaultCoffeeMessage}")
    private String defaultCoffeeMessage;

    public void makeCoffee() {
        System.out.println(defaultCoffeeMessage);
    }
}
