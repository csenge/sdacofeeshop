package edu.sda.CoffeShop;

import edu.sda.CoffeShop.controller.CoffeeController;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class Main {

    @Autowired
    CoffeeController coffeeController;

    public static void main(String[] args) {
            SpringApplication.run(Main.class, args);
    }

    @Bean
    public void testCoffeeMaker(){
      coffeeController.makeCoffee("");
      coffeeController.makeCoffee("latte");
      coffeeController.makeCoffee("machiato");
    }
}
